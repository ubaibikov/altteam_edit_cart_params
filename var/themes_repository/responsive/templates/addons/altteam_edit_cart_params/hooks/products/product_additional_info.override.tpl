<div class="ty-cart-content__sku ty-sku cm-hidden-wrapper{if !$product.product_code} hidden{/if}" id="sku_{$key}">
    {__("sku")}: <span class="cm-reload-{$obj_id}" id="product_code_update_{$obj_id}">{$product.product_code}<!--product_code_update_{$obj_id}--></span>
</div>
{if $product.product_options}
    <div class="cm-reload-{$obj_id} ty-cart-content__options" id="options_update_{$obj_id}">
            {include file="buttons/button.tpl" but_meta="ty-btn" but_role="text" but_text=__("edit_params") but_href="products.view?product_id=`$product.product_id`&key=`$key`"}
    <!--options_update_{$obj_id}--></div>
{/if}