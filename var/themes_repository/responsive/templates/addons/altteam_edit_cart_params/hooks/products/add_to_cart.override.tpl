{if $product.has_options && !$show_product_options && !$details_page}
    {if $but_role == "text"}
        {$opt_but_role="text"}
    {else}
        {$opt_but_role="action"}
    {/if}

    {include file="buttons/button.tpl" but_id="button_cart_`$obj_prefix``$obj_id`" but_text=__("select_options") but_href="products.view?product_id=`$product.product_id`" but_role=$opt_but_role but_name="" but_meta="ty-btn__primary ty-btn__big"}
{else}
    {hook name="products:add_to_cart_but_id"}
        {$_but_id="button_cart_`$obj_prefix``$obj_id`"}
    {/hook}

    {if $extra_button}{$extra_button nofilter}&nbsp;{/if}

    {if $smarty.request.key}
     <div class="cm-reload-{$obj_prefix}{$obj_id}" id="product_features_update_{$obj_prefix}{$obj_id}">
        <input type="hidden" name="key" value="{$smarty.request.key}" />
    </div>
       {include file="buttons/add_to_cart.tpl" but_name="dispatch[checkout.update..`$smarty.request.key`]" but_text=__("save_params") but_href="checkout.cart"}
    {else}    
        {include file="buttons/add_to_cart.tpl" but_id=$_but_id but_name="dispatch[checkout.add..`$obj_id`]" but_role=$but_role block_width=$block_width obj_id=$obj_id product=$product but_meta=$add_to_cart_meta}                
    {/if}

    {assign var="cart_button_exists" value=true}
{/if}