<?php
use Tygh\Tygh;
use Tygh\Registry;
use Tygh\BlockManager\ProductTabs;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

if ($mode == 'view' || $mode == 'quick_view') {
    if (isset( $_REQUEST['key'])) {
        
        $key = $_REQUEST['key'];

        $product = Tygh::$app['view']->getTemplateVars('product');
        $session_cart = Tygh::$app['session']['cart']['products'][$key];

        foreach (array_keys($session_cart['product_options']) as $option_id) {
            $product['product_options'][$option_id]['value'] = $session_cart['product_options'][$option_id];
        }

        $product['selected_amount'] = $session_cart['amount'];

        Tygh::$app['view']->assign('product', $product);
    }

}
