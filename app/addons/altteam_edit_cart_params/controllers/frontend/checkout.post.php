<?php


use Tygh\Tygh;
use Tygh\Registry;
use Tygh\BlockManager\ProductTabs;

defined('BOOTSTRAP') or die('Access denied');

$cart = &Tygh::$app['session']['cart'];
$auth =  &Tygh::$app['session']['auth'];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($mode == 'update') {


        if ($_REQUEST['product_data']) $_REQUEST['cart_edit'] = $_REQUEST['product_data'];


        if (!empty($_REQUEST['cart_edit'])) {

            $key = $_REQUEST['key'];

            foreach ($_REQUEST['cart_edit'] as $cart_edit) {
                $cart['products'][$key]['amount'] = $cart_edit['amount'];
                foreach (array_keys($cart_edit['product_options']) as $option_id) {
                    if ($cart['products'][$key]['product_options'][$option_id] != $cart_edit['product_options'][$option_id]) {
                        fn_add_product_to_cart($_REQUEST['cart_edit'], $cart, $auth, true);
                    }
                }
            }

            fn_save_cart_content($cart, $auth['user_id']);
        }

        unset($cart['product_groups']);

        fn_set_notification('N', __('notice'), __('text_products_updated_successfully'));

        if (!empty($cart['chosen_shipping'])) {
            $cart['calculate_shipping'] = true;
        }

        $cart['recalculate'] = true;

        return [CONTROLLER_STATUS_OK, 'checkout.' . $_REQUEST['redirect_mode']];
    }
}
